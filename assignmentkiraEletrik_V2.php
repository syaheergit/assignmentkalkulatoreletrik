<!DOCTYPE html>
<html>
<head>
  <title>Assignment Calculator Electricity Bill</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
</head>
<body>
  <div class="main container">
    <h1>Assignment Calculator Electricity Bill</h1>
    <div class="container">
    <form method="post">
      <div class="form-group">
        <label for="voltage">Voltage:</label>
        <input type="number" step="0.01" class="form-control" id="voltage" name="voltage" required>
        <label>Voltage (V)</label>
      </div>
      <div class="form-group">
        <label for="current">Current:</label>
        <input type="number" step="0.01" class="form-control" id="current" name="current" required>
        <label>Ampere (A)</label>
      </div>
      <div class="form-group">
        <label for="rate">Current Rate:</label>
        <input type="number" step="0.01" class="form-control" id="rate" name="rate" required>
        <label>sen/kWh</label>
      </div>
      <button type="submit" class="btn btn-primary">Calculate</button>
    </form>
    <br>
    <?php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  $voltage = $_POST['voltage'];
  $current = $_POST['current'];
  $rate = $_POST['rate'];

  function calculateBill($voltage, $current, $rate) {
    $power = ($voltage * $current)/1000;
    $totalPerHour = [];
    $totalPerDay = 0;

    for ($hour = 1; $hour <= 24; $hour++) {
      $energyHour = ($power * $hour * 1000)/1000;
      $totalChargePerHour = number_format($energyHour * ($rate / 100), 2);
      $totalPerHour[$hour] = ['energy' => $energyHour, 'charge' => $totalChargePerHour];
      $totalPerDay += $totalChargePerHour;
    }

    return [$power, $totalPerHour, $totalPerDay];
  }

  [$power, $totalChargePerHour, $totalChargePerDay] = calculateBill($voltage, $current, $rate);

  echo "<div class='card'>";
  echo "<div class='card-body'>";
  echo "<h3 class='card-title'>Result:</h3>";
  echo "<p class='card-text'><strong>Power:</strong> " . $power . " kw</p>";
  echo "<p class='card-text'><strong>Rate:</strong> RM " . $rate/100 . "</p>";
  
  echo "<h4 class='card-subtitle mb-2'>24 Hours Output:</h4>";
  echo "<table class='table'>";
  echo "<thead>";
  echo "<tr>";
  echo "<th scope='col'>Hour</th>";
  echo "<th scope='col'>Energy (kWh)</th>";
  echo "<th scope='col'>Total (RM)</th>";
  echo "</tr>";
  echo "</thead>";
  echo "<tbody>";
  foreach ($totalChargePerHour as $hour => $data) {
    echo "<tr>";
    echo "<th scope='row'>" . $hour . "</th>";
    echo "<td>" . $data['energy'] . "</td>";
    echo "<td>" . $data['charge'] . "</td>";
    echo "</tr>";
  }
  echo "</tbody>";
  echo "</table>";

  echo "</div>";
  echo "</div>";
}
?>
    </div>
  </div>
</body>
</html>

<style>
  label {
    color: #B4886B;
    font-weight: bold;
    display: block;
  }

  body {
      background-color: #ADD8E6;
    }

  .main-container {
    background-color: #FFFFFF;
    padding: 20px;
    margin-top: 30px;
    border-radius: 5px;
    box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
  }
</style>
